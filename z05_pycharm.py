"""
Shortcuts

Strg + Alt + L => Code neu formatieren
Shift + F6 => Etwas umbennenen
Alt + Links-Klick => Mehrere Cursor setzen
Strg + Alt + V => Wert als Variable extrahieren
Strg + Alt + M => Markierte Zeilen als Funktion (oder Methode) speichern

Strg + Leertaste => Triggert die Code Completion
Strg + J => Templates

Strg + Shift + A => Suche nach Actions
"""


def test():
    print(my_text)
    print(my_text)
    print(my_text)


def double(zahl):
    return zahl * 2


wort = 'Hallo'

my_text = 'multi line test text'

namen = ['b', 'c', 'd']

for i, w in enumerate(wort):
    print(i, w)

test()
double(23)
print(wort)
