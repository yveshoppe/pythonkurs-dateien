from random import random
from abc import ABC, abstractmethod


class MovingObject(ABC):
    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def move(self):
        pass


class Auto(MovingObject):
    def __init__(self, hersteller, ps):
        self.hersteller = hersteller
        self.ps = ps
        self.pos = 0

    def fahren(self):
        self.pos = self.ps * random()


class SchneckeZuSchnell(ValueError):
    pass


class Snail:
    def __init__(self, name, race, max_v):
        self.name = name
        self.race = race

        if max_v > 30:
            raise SchneckeZuSchnell('Glaub ich nicht!')

        self.max_v = max_v
        self.position = 0

    def crawl(self):
        self.position += self.max_v * random()

    def __str__(self):
        # toString
        return "Name: {} ({}) -- Position {}".format(self.name, self.race, self.position)


class Race:
    def __init__(self, name, length):
        self.name = name
        self.length = length
        self._attendees = []
        # Sinnlose Variable :-)
        self.attendees_count = 0

    def add_snail(self, snail):
        if not isinstance(snail, Snail):
            raise ValueError('Snails only')

        self._attendees.append(snail)
        self.attendees_count += 1

    def remove_snail(self, snail_name):
        for s in self._attendees:
            if s.name == snail_name:
                self._attendees.remove(s)
                self.attendees_count -= 1

    def let_snails_crawl(self):
        for s in self._attendees:
            s.crawl()

    def get_winner(self):
        for s in self._attendees:
            if s.position >= self.length:
                return s

        return None

    def run(self):
        while self.get_winner() is None:
            self.let_snails_crawl()

        return self.get_winner()


try:
    rudi = Snail('Rudi', 'Weinberg', 40)
    fritz = Snail('Fritz', 'Weinberg', 11)
except SchneckeZuSchnell:
    print('Die Schnecke ist zu schnell')

# Ruft die __str__ Methode intern auf
print(rudi)
