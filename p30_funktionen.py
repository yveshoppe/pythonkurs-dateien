def print_hello():
    print('hello')


print('ausserhalb')
print_hello()


def print_wort(wort, wort2):
    print('Wort', wort, wort2)


print_wort('welt', 'python')


# public String getSinn() {..}
def get_sinn():
    return 42


print(get_sinn())


def call_func(fun):
    fun()


call_func(print_hello)


def is_sinn(zahl):
    if zahl == 42:
        return True

    if zahl == 666:
        raise ValueError('?!?!')

    # Die Ebene ist ideal
    return False


is_sinn(42)

"""
Regeln für Funktionen

- Fangen mit def an
- Kleingeschrieben, kein camelCase, wieder mit _ trennen
- Nur ASCII, keine Sonderzeichen, ausser _
- Bennenung deutet an was die Funktion tut, ergo:
  * verb, get_, is_, has_, set_
- Englisch
"""
