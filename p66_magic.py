class EqClass:
    def __init__(self, zahl):
        self.zahl = zahl

    def __eq__(self, other):
        if not isinstance(other, EqClass):
            return False

        if self.zahl == other.zahl:
            return True

        return False


a1 = EqClass(42)
a2 = EqClass(42)

print(a1 == a2)
print(a1.zahl == a2.zahl)
print(a1 == 42)

print(type(42))


class Kunde:
    def __init__(self, vorname, nachname):
        self.vorname = vorname
        self.nachname = nachname
        self.id = 42

    def __int__(self):
        return self.id

    def __str__(self):
        # toString()
        return "{} {}".format(self.vorname, self.nachname)


k1 = Kunde('Bernd', 'Huber')

print(str(k1))
print(int(k1))


with open('a.txt') as f:
    print(f.readlines())

# AB HIER ist f geschlossen


class ContextManager:
    def __init__(self, file_name):
        self.file_name = file_name

    def __enter__(self):
        print('Opening file', self.file_name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('Exit file', self.file_name)


with ContextManager('test.txt') as c:
    print('c is open')

print('ENDE')


class AddClass:
    def __init__(self, zahl):
        self.zahl = zahl

    # +-*/ ** % // round
    def __add__(self, other):
        self.zahl += other
        return self

    def __getattr__(self, item):
        return 42


a1 = AddClass(3)
a1 + 4 + 3
a1 += 4
a1 = a1 + 5 + 7 + 8

print(a1.zahl)

print(a1.nicht_existent)
print(a1.nicht_existent2323)

print(getattr(a1, 'asdf'))
