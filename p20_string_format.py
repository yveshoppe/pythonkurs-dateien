zahl = 42
text = 'Der Sinn ist ' + str(zahl)
text_fmt = 'Der Sinn ist {} und {:.2f}'

print(text_fmt.format(zahl, 42.424242))
print(text_fmt)

test = 'Hallo {name}, du bist {alter}'
print(test.format(name='Bernd', alter=42))