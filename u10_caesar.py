ABC = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '

# word = input('Wort?')
# cipher = input('Cipher?')

# if not cipher.isnumeric():
#     print('Error: Cipher needs to be a number')
#     exit(42)


def crypt(word, cypher):
    encrypted = ''

    for letter in word:
        letter_in_abc = ABC.index(letter)
        new_pos = letter_in_abc + cypher

        if new_pos > len(ABC) - 1:
            new_pos -= len(ABC)

        encrypted += ABC[new_pos]

    return encrypted


print(crypt('Hallo Welt', 3))