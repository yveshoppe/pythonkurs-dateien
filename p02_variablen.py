import sys

"""
Datentypen

- int
- float
- string
- bool
- function
- class
- object (of class)
- NoneType
"""

# byte kleinst = 2^8 = 256 --- 0-255, -128 bis +127
# short kleiner = 2^16 = 65536
# int zahl = 42; 2^32 (uint int)
# long grosseZahl = 99999999999999999999l; 2^64
zahl = 42

print(zahl)
print(type(zahl))

x = 9999999999999999999999999999999999999999

print(x * 2)
print(sys.maxsize)

# double pi = 3.14159; 2^64
# float pi = 3.14159f; 2^32
pi = 3.14159

# float 2^64
print(type(pi))

grosse_komm_zahl = 3.123456789123456789123456789
print(grosse_komm_zahl)

print(0.5 + 0.5)
print(0.5 + 0.3 + 0.2)
print(0.5 + 0.2 + 0.2 + 0.1)
print(0.5 + 0.2 + 0.1)

# Zeichen
# String.. In Python keinen char
# String wort = "hallo";
wort = 'hallo'

print(wort)
print(type(wort))

# Boolean
wahr = True
falsch = False

print(wahr)
print(type(wahr))

# Collections
# String[] namen = {"Bernd", "Maria", "Tanja"}
# List (Array)
namen = ['Bernd', 'Maria', 'Tanja']

print(namen)
print(type(namen))

# String test = null;
print(None)
print(type(None))

# Class
print(type(int))

# Function
print(type(print))


class Test:
    pass


# Objekt
# new Test()
test = Test()

print(type(test))

z = 42

# Typ-Umwandling (Casting) mit Funktionen
print('Zahl: ' + str(z))

var_name = 'werte'

print(var_name)

konventionen = 42

# SCHLECHT
müller = 'Müller'

print(müller)

# Falsch
camelCase = 42

# Richtig
kein_camel_case = 42

# const, define, public static final
PI = 3.14159
KONSTANTE = 'SOLL NICHT GEÄNDERT WERDEN'

test = 42
Test = 43

print(test)
print(Test)

"""
Variablen Namen:

- Müssen mit einem Buchstaben oder einem Underscore beginnen
- Underscore -> Spezielle Bedeutung
- Nur ASCII und als EINZIGES Sonderzeichen den Underscore
- Kein camelCase, sondern _
- Komplett kleingeschrieben
- Keine Konstanten (werden angedeutet mit CAPSLOCK)
- Unterscheidung zwischen Klein- und Großschreibung
"""
