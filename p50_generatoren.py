def generator_func():
    if True:
        yield 10

    yield 2 * 10
    yield print('test')

    for i in range(10):
        yield i


gen = generator_func()

print(next(gen))
print(next(gen))
print(next(gen))
# print(next(gen))
# print(next(gen))
