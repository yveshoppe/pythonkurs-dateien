"""
class Point {
    public int x = 10;
    public int y = 12;
}
"""


class Point:
    x = 10
    y = 12


# Point p1 = new Point();
p1 = Point()
p2 = Point()

p1.x = 20
p2.x = 44

print(p1.x, p2.x)
print(id(p1), id(p2))

# Attribute dynamisch einem Objekt hinzufügen
p1.z = 55

print(p1.z)

"""
Konvention:

- KlassenNamen verwenden CapsCase
- keine Unterstriche im Klassennamen

"""
