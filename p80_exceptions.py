# Beendet unser Program

try:
    namen = ['a']
    print(namen[3])
    print(10 / 0)
except ZeroDivisionError:
    print('FEHLER: Nicht durch 0 dividieren')
except IndexError as e:
    print('ArrayOutofBounds error')
    print(e)
except Exception:
    print('irgendein anderer Fehler')
"""
try {
    sysout(10 / 0);
} catch (Exception e) {
    sysout('Fehler')
}

"""

print('Hallo')
