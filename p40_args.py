def addieren(zahl1, zahl2, zahl3=0, zahl4=0):
    return zahl1 + zahl2 + zahl3 + zahl4


addieren(3, 4)
addieren(3, 4, 5)
addieren(3, 4, 5, 6)


def addieren_args(*zahlen, test):
    print(zahlen, test)
    ergebnis = 0

    for zahl in zahlen:
        ergebnis += zahl

    return ergebnis


print(addieren_args(1, test=23))
print(addieren_args(1, 2, test=22))
print(addieren_args(1, 2, 3, test=44))

print(1, 2, 3)

print('test', 't2', sep=',')

print(sum([2,3,4]))



