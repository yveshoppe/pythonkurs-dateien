"""
class A {
    public int zahl = 42;
    protected int prot = 33;
}


"""

class A:
    def __init__(self):
        self.zahl = 42
        self._prot = 33
        self.__priv = 22

    def _verstecke_von_aussen(self):
        return 42


a1 = A()

print(a1.zahl)
print(a1._prot)
# Class Mangeling
print(a1._A__priv)

print(a1._verstecke_von_aussen())

