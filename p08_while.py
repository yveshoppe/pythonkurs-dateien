# Zähler
i = 0

# Bedingung
while i < 10:
    print('Durchlauf', i)
    # Inkrement
    i += 1


zahl = 100000
counter = 0


while zahl > 9:
    zahl = zahl / 3
    # print(zahl)
    counter += 1


print('Hat', counter, 'gedauert')


# Endlosschleifen
while True:
    antwort = input('Zahl? ')

    if antwort == '42':
        break


print('Du hast den Sinn gefunden')
