import json

kunde = {
    "vorname": "Bernd",
    "nachname": "Huber",
}

json_string = json.dumps(kunde)
k = json.loads(json_string)

print(k.get('vorname'))


class Kunde:
    def __init__(self, vorname, nachname):
        self.vorname = vorname
        self.nachname = nachname
        self.__password = 'test'


k1 = Kunde('Tanja', 'Meier')

# Behelfslösung vars
print(json.dumps(vars(k1)))
