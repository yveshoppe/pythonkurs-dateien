"""
public String getName(vorname) {
    // ..
}

public String getName(vorname, nachname) {
    // ..
}
"""


def print_name(vorname, nachname=''):
    # public void print_name
    if nachname:
        print('Hallo', vorname, nachname)
        return

    print('Hallo', vorname)


print_name('Bernd')
print_name('Bernd', 'Huber')


def print_kunde(vorname, alter, nachname=''):
    pass


