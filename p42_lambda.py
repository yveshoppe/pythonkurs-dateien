doubler = lambda x: x * 2


def doubler_func(x):
    return x * 2


print(doubler(10))

addition = lambda x, y: x + y

print(addition(3, 4))


def sinnlos(fun, zahl):
    return fun(zahl)


print(sinnlos(lambda x: x * 3, 3))

print((lambda x: x / 0)(4))
