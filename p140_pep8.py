from logging import FileHandler
import Snail
from Snail import Snail

Snail()

# Fehler
Snail.Snail()

# BDFL -> Guido van Rossum

# Maximal-Zeilenlänge: 79
# Kommentare: 72


def very_long_function_name(
        ein_langer_parameter1,
        ein_langer_parameter2,
        ein_langer_parameter3,
):
    pass


my_list = [
    'test',
    'test',
    'test',
    'test',
    'test',
    'test',
]

# Leerzeichen vs Tabs?
# -> Leerzeichen (4)
# ES MUSS DURCHGEHEND das selbe genommen werden

# Python2: ASCII
# Python3: UTF-8

print('KEINE LEERZEICHEN VOR UND NACH KLAMMERN')

# Nicht auf die selbe Ebene einrücken
x = 42
sinn = 42


class A:
    pass


# 2 Leerzeilen vor und nach einer Klasse und einer globalen Funktion


class B:
    def test(self):
        pass

    def test2(self):
        pass

# 2 frei


# Keine Lambdas nach Möglichkeit
verdoppeln = lambda x: x * 2


# NO GO
import sys, os

# Okay
import sys
import os

from sys import maxsize, path, argv

"""
imports NUR am Dateianfang und nicht dynamisch
"""

# No GO
if True:
    import p10_listen


# Erlaubt
try:
    import requests
except:
    print('Gibts auf der Plaftorm')



words = ('python',)

# Kommentare

# Immer in ganzen Sätzen
# Validator verlangt das der erste Buchstabe groß ist
# kein gültiger Kommentar
zahl = 42  # Inline-Comments selten einsetzen und 2 Spaces davor

# Namens Konventionen

# Kein camelCase, sondern _ bei Variablen, Funktionen und Methoden
# protected _test
# private __test
# magic __test__
# I, O, l nicht als Zähler verwenden
# ASCII


def do_something_with_class(class_):
    pass


# Klassen
# CapsCase, groß-geschrieben und CamelCase
# In Methoden, muss self immer der erste Parameter sein
# Getter und Setter sind total typisch (@Property)

# is oder not oder is not in Kombination mit None und Boolean benutzen (Nicht == oder !=)
# is True oder is not True

print('Hello',
      'welt')

with open('/ein/langer/pfad') as t,\
        open('/ein/langer/pfad') as t2:
    pass

# Jython
# "test".join("test")