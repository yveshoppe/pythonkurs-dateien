kunde = {
    'vorname': 'Bernd',
    'nachname': 'Meier',
    'alter': 42,
    'adresse': {
        'strasse': 'Teststr. 42',
        'plz': '23233'
    },
    'neu': 'neu',
}

print(kunde['vorname'])
print(kunde['nachname'])

print('test')

# print(kunde['gibts_nicht'])
# KeyError vermeiden mit .get:
print(kunde.get('gibts_nicht', 'Nicht definiert'))

print(kunde.keys())
print(kunde.values())

for i in kunde:
    print(i, '=', kunde.get(i))
