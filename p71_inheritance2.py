from random import random


class A:
    def __init__(self, zahl):
        self.zahl = zahl
        print('Konstrukt A')

    def print_a(self):
        print('A')

    def print_morgen(self):
        print('morgen')


class B(A):
    def __init__(self):
        print('B Konstruktor')
        super().__init__(42)
        print('B weiter')

    def print_a(self):
        print('A überschrieben')
        self.print_morgen()
        super().print_a()


b1 = B()
b1.print_a()


class ComplexA:
    def __init__(self, a):
        self.a = a

    def print_a(self):
        print(self.a)


class ComplexB(ComplexA):
    def __init__(self, a, b):
        super().__init__(a)
        self.b = b

    def print_b(self):
        print(self.b)


b1 = ComplexB(42, 23)
b1.print_a()
b1.print_b()


class Kfz:
    def __init__(self, hersteller, model, ps):
        self.hersteller = hersteller
        self.model = model
        self.ps = ps

    def fahren(self):
        return random() * self.ps


class Auto(Kfz):
    pass


class Motorad(Kfz):
    def __init__(self, hersteller, model, ps, hubraum):
        self.hubraum = hubraum
        super().__init__(hersteller, model, ps)

    def fahren(self):
        return self.hubraum / self.ps * 5 * random()


class Lkw(Kfz):
    def fahren(self):
        return self.ps / 3 * random()

