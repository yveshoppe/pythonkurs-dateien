def change_zahl(zahl):
    zahl = 23


# Globale Variable
zahl = 42

change_zahl(zahl)

print(zahl)


def change_zahl2():
    # global zahl
    # Lokale Variable
    print(zahl)
    # zahl = 3


change_zahl2()


print(zahl)

"""
Global:

* Erlaubt mir den schreibenden Zugriff auf eine äussere "globale" Variable
* Lesender Zugriff ohne möglich (SOLANGE die Variable nicht überdeckt wird)
"""


def change_string(wort):
    wort = 'Welt'


wort = 'Hallo'

change_string(wort)

print(wort)


def change_list(namen):
    namen[1] = 'GEÄNDERT'


namen = ['Maria', 'Tanja', 'Bernd']

change_list(namen)

print(namen)


def change_obj(a):
    a.wort = 33


class A:
    pass


a1 = A()
a1.zahl = 42

change_obj(a1)

print(a1.zahl)


"""
Primitive Datentypen (Einfache) (int, float, bool, String)
-> werden kopiert

Komplexe Datentypen (list, tuple, dict, object)
-> werden referenziert!
"""





