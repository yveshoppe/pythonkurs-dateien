zahl = 666

# Keine Klammern, ein doppelpunkt um den Block anzufangen
if zahl == 42:
    print('Zahl ist 42')
    print('Immer noch in der Bedingung')
elif zahl == 666:
    print('?!?!!')
elif zahl > 100:
    print('Der Sinn ist nicht so komplex')
else:
    print('Du bist noch auf der Suche')


print('IMMER')

"""
if (zahl == 42) {
    // ...
}

"""

alter = 20

"""
3 Tickets
- Kinderticket (0-17)
- Erwachsenenticket
- Seniorenticket (ab 65)
"""

if alter > 64:
    print('Senioren')
elif alter < 18:
    print('Kinder')
else:
    print('Erwachsen')

"""
Schaltjahr?

- Sind durch 4 teilbar
- Sind nicht durch 100teilbar ausser sie sind auch durch 400teilbar
- Schaltjahre: 2020, 2016, 2000
- Keine: 2021, 2019, 2022, 1900
"""

jahr = int(input('Jahr? '))

if jahr % 4 != 0:
    print('Kein Schaltjahr')
elif jahr % 100 == 0 and jahr % 400 != 0:
    print('Kein Schaltjahr')
else:
    print('Schaltjahr')

