"""
class Kunde {
    public String vorname = '';
    public String nachname = '';

    public void printKunde() {
        System.out.println(this.vorname + ' ' + this.nachname);
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }
}
"""

class Kunde:
    # Attribute
    vorname = ''
    nachname = ''

    # Methoden
    def print_kunde(self):
        # self = this
        print(self.vorname, self.nachname)

    def set_nachname(self, nachname):
        self.nachname = nachname


bernd = Kunde()
bernd.vorname = 'Bernd'
bernd.nachname = 'Huber'
bernd.print_kunde()

bernd.set_nachname('Maier')
bernd.print_kunde()
