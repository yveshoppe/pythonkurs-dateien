# Strings sind wie Listen (Array) behandelbar
text = 'Hallo Welt'

# Die ersten 5 Zeichen
print(text[0:5])

# Ab dem 6ten Zeichen
print(text[6:])

# Zeichen mit einem Interval von 2
print(text[0::2])

print(text[::-1])

# Mit drei Anführungszeichen (' oder ")
mehrzeilig = '''Guten
Morgen
Montag'''

print(mehrzeilig)

text = 'Hallo Welt'

# String Methoden
text = text.replace('Welt', 'Python')

print(text)

print(text.upper())
print(text.lower())

print(text.lower().endswith('python'))
print(text.lower().startswith('hallo'))

print('42'.isnumeric())
print(' 42 '.strip())
print('zwei wörter geteilt'.split(' '))

print(len(text))

# "indexOf", "contains" in python
print('Python' in text)

# Nicht direkt benutzen
# print('Python'.__contains__())
