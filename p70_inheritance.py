class A:
    def print_a(self):
        print('A')


# class B extends A
class B(A):
    def print_b(self):
        print('B')


b1 = B()
b1.print_a()

print(isinstance(b1, A))
print(isinstance(b1, B))


class C(B):
    def print_c(self):
        print('C')


c1 = C()
c1.print_a()
c1.print_b()
c1.print_c()

print(isinstance(c1, A))
print(isinstance(c1, B))
print(isinstance(c1, C))
