import math

# Operatoren

# Rechenoperatoren

# +-/*

# Division (Kommt immer eine Kommazahl raus)
print(10 / 3)

# Modulo ? Frage nach dem Restwert (Ganzen)
print(11 % 4)
print(12 % 4)

print(10 % 2)
print(11 % 2)

# Potenz / Fakultät

print(3 ** 3)

# Floor
print(11 // 3)

# Inkrementoren / Dekrementoren (gibt es in Python nicht)
# int i = 0;
# i++;
i = 0
i = i + 1

# Kurzformen
i += 1  # i = i + 1
i *= 2  # i = i * 2

# Zuweisungsoperator
# a = b

# Verknüpfungsoperator
# +
print('Hallo' + ' Welt')


# Vergleichsoperatoren
# Liefert immer einen Bool zurück und prüfen den Datentyp
print(42 == 42)
print(42 != 42)
print(42 > 42)
print(42 < 42)
print(42 >= 42)
print(42 <= 42)

# ?
# == überprüft Inhalt und DATENTYP
print(42 == '42')
# 01100001 (0x61) -- 01110001 (0x62)
print('a' > 'b')
print('a' < 'b')
# 0x42 -- 0x61
print('Bernd' > 'adam')
print('----')

# Logischen Operatoren
# and -> (&&)
"""
True && False = False
True && True = True
False && True = False
False && False = False
"""
print(42 == 42 and 23 > 42)

"""
True && False = True
True && True = True
True && False = True
False && False = False
"""
print(42 == 42 or 23 > 42)

# ! Logische Nicht
print(not True)
print(not False)

zahl = None

print(zahl is None)
print(zahl is not None)

print(zahl == None)

print('5 min Pause ' * 3)

