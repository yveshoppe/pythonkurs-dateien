class A:
    zahl = 42

    def __eq__(self, other):
        if self.zahl == other.wort:
            return True

        return False


a1 = A()
a2 = A()

print(a1 == a2)


class B:
    zahl = 10

    def __add__(self, other):
        self.zahl += other


b1 = B()
b1 + 3

print(b1.zahl)

b1 + 24
print(b1.zahl)


