class A:
    zahl = 42

    def print_a(self):
        print('a')

    def print_b(self):
        print('b')

    def print_c(self):
        print('c')


a1 = A()
eingabe = 'b'

if hasattr(a1, 'zahl'):
    print(getattr(a1, 'zahl'))

# if eingabe == 'a':
#     a1.print_a()
# elif eingabe == 'b':
#     a1.print_b()
# # :..

# func = getattr(a1, 'print_' + eingabe)
# func()

getattr(a1, 'print_' + eingabe)()
