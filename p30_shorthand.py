# int zahl = 42;
# a = zahl == 42 ? 'sinn' : falsch;

zahl = 42

# Nach Möglichkeit nicht verwenden (schwerer lesbar)
a = 'sinn' if zahl == 42 else 'falsch'

if zahl == 42:
    a = 'sinn'
else:
    a = 'falsch'

print(a)

jahr = 2020

schaltjahr = 'Schaltjahr' if jahr % 4 == 0 and (jahr % 100 != 0 and jahr % 400 != 0) else 'Keins'
