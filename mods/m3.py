def is_sinn(zahl):
    return zahl == 42


if __name__ == '__main__':
    # Dieser Code hier, wird NUR ausgeführt, wenn mein Modul
    # direkt als Programm verwendet wird.
    print(__name__)
    print(is_sinn(42))
    print(is_sinn(33))
