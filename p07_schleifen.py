# for (int i = 0; i < 10; i++) {
# // ..
# }

# Iterator range
for i in range(10):
    print('Durchlauf', i)
    i += 2
    print(i)


# (0, 1, 2, 3, ..., 9)
print(range(10))

# for (int i = 0; i < 10; i += 2) {
for i in range(0, 10, 2):
    print(i)

    #       0         1         2
namen = ['Bernd', 'Dieter', 'Tanja']

for name in namen:
    print(name)

for n in range(len(namen)):
    print(namen[n])


for i in []:
    print(i)
else:
    # Wird immer am Ende ausgeführt
    print('Finish')


