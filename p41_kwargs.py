import logging


def print_kunde(**kunde):
    print(kunde.get('vorname', ''))
    print(kunde.get('nachname', ''))
    print(kunde.get('alter', 'Unbekannt'))


print_kunde(nachname='Huber', vorname='Bernd', alter=42)


def print_kunde_dict(kunde):
    print(kunde.get('vorname', ''))
    print(kunde.get('nachname', ''))
    print(kunde.get('alter', 'Unbekannt'))


kunde = {
    'vorname': 'Bernd',
    'nachname': 'Huber'
}
print_kunde_dict(kunde)
print_kunde()

logging.basicConfig(filename='test.log', level=logging.DEBUG)

