from abc import ABC, abstractmethod


class NeedsPrint(ABC):
    def print_a(self):
        print('a')

    @abstractmethod
    def print_required(self):
        pass


class PrintCl(NeedsPrint):
    def print_required(self):
        print('Req')


p1 = PrintCl()

