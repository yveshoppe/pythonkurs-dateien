#         0, 1, 2
zahlen = [3, 4, 5]

print(zahlen[1])

zahlen[1] = 42

print(zahlen)

# In Python kann man Listen miteinander addieren
zahlen += [3]

print(zahlen)

# Ein neues Element anfügen
zahlen.append(7)

print(zahlen)

# Eine Liste in der Liste (Mehrdimensional)
zahlen.append([23])

print(zahlen)

print(len(zahlen))

for i in zahlen:
    # i ist nicht manipulierbar (eine Kopie, bzw. ein immutable)
    print(type(i))
    # Besser isinstance verwenden
    if type(i) == int:
        i += 23
        print('i neu', i)

    if isinstance(i, int):
        i += 23
        print('i neu', i)

print(zahlen)

# list.pop() -> Entfernt das letzte Element der Liste
print(zahlen.pop())
print(zahlen)

# Entferne Element an index
print(zahlen.pop(0))
print(zahlen)

# Remove entferne den WERT
zahlen.remove(42)
print(zahlen)

# Magische Methode
print(zahlen.__len__())
print(len(zahlen))


class Montag:
    def __len__(self):
        return 3


m = Montag()
print(len(m))

zahlen.sort()

print(zahlen)

zahlen.reverse()

print(zahlen)



