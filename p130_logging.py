import logging

fh = logging.FileHandler(filename='warning.log')
fh.setLevel(logging.WARNING)

sh = logging.StreamHandler()
sh.setLevel(logging.DEBUG)

logging.basicConfig(level=logging.DEBUG, handlers=(fh, sh))

logging.info('Hallo Welt')
logging.warning('Guten Morgen')

logging.info('Hallo Welt')
logging.warning('Später')
# logger = logging.getLogger("Logger")

