import mods.m1
import mods.sub.s1
# Alias
import mods.sub.s1 as s1

import random as random_pkg
from random import random, randint, randrange

# randint => rand_int, rand_range

# Importiere nur einen Teil in meinen NAMESPACE
from mods.sub.s1 import z

print('Test')

print(mods.m1.get_sinn())
print(mods.sub.s1.z)
print(s1.z)
print(z)
print(random.random())