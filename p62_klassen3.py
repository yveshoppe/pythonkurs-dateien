from random import random

"""
class Auto {
    public String hersteller;
    public String model;
    public String ps;

    public Auto(String hersteller, String model, int ps) {
        this.hersteller = hersteller;
        this.model = model;
        this.ps = ps;
    }
}

"""


class Auto:
    # Konstruktor
    def __init__(self, hersteller, model, ps):
        # Attribute
        self.hersteller = hersteller
        self.model = model
        self.ps = ps
        self.farbe = 'gelb'
        self.xy = 0

    # Methoden
    def fahren(self):
        return self.ps * random()

    def add_attr(self):
        self.xy = 42


lamborgini = Auto('Matchbox', 'Urus', 640)
trabant = Auto('Trabant', 'P50', 20)
audi = Auto('Audi', 'A5', 300)

trabant.baujahr = 1990

# Auto[] autos = new Auto[3];
for auto in [lamborgini, trabant, audi, 42]:
    # if hasattr(auto, 'fahren')
    if not isinstance(auto, Auto):
        continue

    print(auto.fahren())


