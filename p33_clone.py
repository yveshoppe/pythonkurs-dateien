import copy

namen = ['Maria', 'Tanja', 'Bernd']

# REFERENZIERT
namen2 = namen

namen2[1] = 'GEÄNDERT'

print(namen)
print(id(namen), id(namen2))
print(namen == namen2)

z1 = 42

# KOPIE
z2 = z1

z2 = 33

print(z1)

namen3 = copy.copy(namen)

namen3[0] = 'GEÄNDERT'

print(namen)
print(namen3)
print(id(namen), id(namen3))

zahlen = [42, [2, 3], [4, 5]]

# Flache KOPIE
zahlen2 = copy.copy(zahlen)

zahlen2[0] = 55

print(zahlen2, zahlen)
print(id(zahlen2), id(zahlen))

zahlen2[1][1] = 99

print(zahlen2, zahlen)
print(id(zahlen2[1]), id(zahlen[1]))

# Kopiert auch untere "Ebenen"
zahlen3 = copy.deepcopy(zahlen)
zahlen3[2][1] = 999
print(zahlen3, zahlen)
