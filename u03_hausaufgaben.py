wort = input('Wort? ').strip()

# Rentner = renteR

umgedreht = wort[::-1].lower()

if umgedreht == wort.lower():
    print(wort, 'ist ein Palindrom')
else:
    print(wort, 'ist kein Palindrom')

# Random
from random import random

zufall = random()

if zufall < 0.42:
    print('Gewonnen')


"""
i_range = 11
j_range = 11

a = [[0] * (j_range - 1) for i in range(1, i_range)]

for i in range(1, i_range):
    for j in range(1, j_range):
        print(j, 'x', i, '=', i * j)
        a[j - 1][i - 1] = i * j

for row in a:
    print(row)

"""

ergebnisse = []

for x in range(1, 11):
    for y in range(1, 11):
        ergebnis = x * y
        print(x, 'x', y, '=', ergebnis)
        ergebnisse.append({
            'x': x,
            'y': y,
            'ergebnis': ergebnis,
        })
