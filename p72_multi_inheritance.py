class A:
    def __init__(self, a):
        self.a = a

    def print_a(self):
        print('a', self.a)


class B:
    def __init__(self, b):
        self.b = b

    def print_b(self):
        print('b', self.b)


class C(B, A):
    def __init__(self, a, b):
        super().__init__(b)
        self.a = a


c1 = C(42, 43)
c1.print_a()
c1.print_b()

"""
Fazit:

Mehrfachvererbung sollte nur in SONDER-Fällen angewendet werden,
wegen der Probleme mit Konstruktoren und Attributen

"""
