zahlen = (3, 4, 5)
ein_element_im_tuple = (42,)

print(zahlen)
print(zahlen[1])

# zahlen[1] = 44

print(zahlen)

"""
Tuple sind unverändliche (immutable) listen
"""

zahlen += (23,)

print(zahlen)
